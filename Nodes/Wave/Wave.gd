extends Node2D
var amplitude = 4
var frequency = 5
var scale = 20
var Level_of_detail = 200
var width
var height
var color = Color(1.0, 0.0, 0.0)
var position_x = 0
var position_y = 200

func _draw():	
	draw_circle_arc(color)
	
func _ready():
	if !width || !height:
		var size = get_viewport_rect().size
		if !width:
			width = size.x
		if !height:
			height = size.y
	set_process(true)
	
func _process(delta):
	update()

func waveFunc(x):
	return sin(frequency*x)*amplitude
	
func draw_circle_arc(color):
	var nb_points = Level_of_detail
	var points_arc = Vector2Array()
	points_arc.push_back(Vector2(0,height))
	var x_offset = width/nb_points
	var colors = ColorArray([color])
	for i in range(nb_points+1):
		var x = (i*x_offset)+position_x
		var y = waveFunc(x)+height-position_y
		var point = Vector2(x-position_x,y)
		points_arc.push_back(point)
	points_arc.push_back(Vector2(nb_points*x_offset,height))
	draw_polygon(points_arc, colors)