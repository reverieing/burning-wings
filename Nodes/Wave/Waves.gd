extends Node2D
onready var Wave = preload("res://Nodes/Wave/Wave.xml")
var waves = []
var screen_bottom_limit = 50
var screen_top_limit = 200
var initial_color = Color(0,0,1)
var final_color = Color(1,1,1)
var waves_number = 10
var position_x = 0
var position_y = 0
var speed = Vector2(0,0)
var amplitude_min = 1.00
var amplitude_max = 10.00
var frequency_divider = 2
var limit_up = 30
var limit_down = 0
var base_height = 40
var reached_limit_up = false
var reached_limit_down = false

func spawn(_waves_number):
	prepare_waves(waves_number)
	set_process(true)

func get_step(_min,_max,total):
	return ((float(_max) - float(_min))/total)

func get_height_offset(total):
	var height_offset = get_step(screen_bottom_limit,screen_top_limit,total)
	return height_offset

func _process(delta):
	var total = waves.size()
	position_x+=speed.x
	position_y+=speed.y
	var height_offset = get_height_offset(total);
	var y = clamp((position_y + height_offset),limit_down,limit_up)
	if y <= limit_down:
		reached_limit_down = true
		reached_limit_up = false
	elif y >= limit_up:
		reached_limit_up = true
		reached_limit_down = false
	else:
		reached_limit_up = false
		reached_limit_down = false
	for i in range(total):
		var wave = waves[i]
		animate_wave(wave,i,total,y)

func prepare_waves(total):
	total = total+1
	var height_offset = get_height_offset(total);
	var amplitude_multiplier = get_step(amplitude_min,amplitude_max,total);
	for i in range(0,total):
		var wave = Wave.instance()
		waves.push_back(wave)
		add_child(wave)
		set_wave(wave,i,total,height_offset,amplitude_multiplier,frequency_divider)
	
func set_wave(wave,i,total,height_offset,amplitude_multiplier,frequency_divider):
	var n = i+1
	var nn = total - i
	var step = (1.00/total)
	wave.frequency = (nn/frequency_divider)+.02
	wave.amplitude = n*(1+(amplitude_multiplier))
	wave.position_y = height_offset * nn
	wave.color = initial_color.linear_interpolate(final_color,(step*nn))
	
func animate_wave(wave,i,total,y):
	var nn = total - i
	wave.position_y = y * nn + base_height
	wave.position_x = position_x * (nn+1)