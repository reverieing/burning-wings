
extends Node2D
var bar
var burn_bar

func _ready():
	bar = get_node("lifebar/bar")
	burn_bar = get_node("burning_meter/burning_bar")

func set_life(life):
	var rect = bar.get_region_rect();
	var new_rect = Rect2(rect.pos.x,rect.pos.y,life, rect.size.y);
	bar.set_region_rect(new_rect);

#burn bar works similar to life bar, but in reverse -- filling it is bad for the player	
func set_burn(burn):
	var rect = burn_bar.get_region_rect()
	var new_rect = Rect2(rect.pos.x, rect.pos.y, burn, rect.size.y);
	burn_bar.set_region_rect(new_rect)