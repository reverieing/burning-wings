var direction = Vector2(1,1);
var velocity = Vector2(1,0);
var speed = Vector2();
export var SPEED_INCREMENT_X = 50;
export var SPEED_INCREMENT_Y = 50;
export var SPEED_MAX_X = 400;
export var SPEED_MAX_Y = 400;
export var LIMIT_UP = 0;
export var LIMIT_DOWN = 750;
var body;
var background;

func _ready():
	set_fixed_process(true)
	body = self.get_node("KinematicBody2D");
	background = self.get_node("TileMap");
	
func _fixed_process(delta):
	
	# Check Y direction
	if(Input.is_action_pressed("Up")):
		direction.y = -1
	else:
		# apply gravity
		direction.y = 1

	# Check X direction
	if(Input.is_action_pressed("Left")):
		direction.x = -1
	elif(Input.is_action_pressed("Right")):
		direction.x = 1
	
	# Calculate X velocity
	velocity.x+=delta * (SPEED_INCREMENT_X * direction.x)
	velocity.x = clamp(velocity.x,0,SPEED_MAX_X)
	
	# Calculate Y Velocity
	if(velocity.x):
		velocity.y+=delta * (SPEED_INCREMENT_Y * direction.y)
	else:
		velocity.y+=delta * (SPEED_INCREMENT_Y * 1)
	velocity.y = clamp(velocity.y,-SPEED_MAX_Y,SPEED_MAX_Y)
	
	# Apply velocity to motion
	var motion = velocity * delta;
	var target = body.get_pos() + motion;
	
	# Check Upper Limit
	if(target.y <= LIMIT_UP):
		motion.y = LIMIT_UP;
		velocity.y = 0;
		target = body.get_pos() + motion
	elif(target.y >= LIMIT_DOWN):
		motion.y = 0;
		velocity.y = 0;
		target.y = LIMIT_DOWN;
	
	# Apply rotation
	#var rot = (atan2(target.x,target.y) + deg2rad(-25))*2
	#rot = clamp(rot,deg2rad(-20),deg2rad(60))
	#body.set_rot(rot)
	
	# Apply motion to body
	body.move(Vector2(0,motion.y))
	background.move_local_x(-motion.x);