var direction = Vector2(1,1);
var velocity = Vector2(100,0);
var speed = Vector2();
var up_was_pressed = false
var down_was_pressed = false
export var GRAVITY = .2;
export var INITIAL_PUSH_UP = 100;
export var INITIAL_PUSH_DOWN = 100;
export var SPEED_INCREMENT_X = 50;
export var SPEED_INCREMENT_Y = 50;
export var SPEED_MAX_X = 800;
export var SPEED_MAX_Y = 400;
export var LIMIT_UP = 0;
export var LIMIT_DOWN = 750;
export var HORIZONTAL_TO_VERTICAL_RATIO = 1000000000;

#player
var body
var health = 100
var burn = 0
#background
var background
#shortcut to access global
var globals
#healthmeter
var lifebar

#Most of this code may be used/adapted elsewhere
var arrow = preload("res://Nodes/Bullet/bullet.xml")
var arrows = 0
var arrow_count = 0
var arrow_speed = 100
var arrow_spawn_speed = 1
var arrow_arr = []
var spawn_timer = 0
#how much to decrement spawn speed by each loop; smaller number = more arrows until threshold is reached
var arrow_spawn_decrement = .005
#limit to how often arrows should spawn; smaller number = more arrows spawned at once before frequency increases and speed resets
var arrow_spawn_threshold = .70
#how many arrows spawn at once
var arrow_spawn_frequency = 1

#animation timer to track animation
var animation_timer = 0;

func _ready():
	set_fixed_process(true)
	body = self.get_node("Player")
	background = self.get_node("TileMap")
	#shortcut for future scene changes to reduce line size
	globals = get_node("/root/global")
	# example of setting life bar: 100 is max
	set_life(health)
	set_burn(burn) 
	
func _fixed_process(delta):
	#player movement
	movement_handler(delta)
	#arrow movement
	arrow_movement(delta)
	#collision detection
	collision_detection()
	#Check player height
	check_height(delta)
	#Game ends when Health hits 0
	track_health()
	
func set_life(life):
	get_node("meter").set_life(life)
	
func set_burn(burn):
	get_node("meter").set_burn(burn)
	
func move_background(speed):
	get_node("BG").speed = speed;
	
#put movement into its own function to make fixed_process less bloated
func movement_handler(delta):
	var first_push_increase = 1
	# Check Y direction
	if(Input.is_action_pressed("Up")):
		down_was_pressed = false;
		if(!up_was_pressed):
			up_was_pressed = true
			first_push_increase = 1+INITIAL_PUSH_UP;
		direction.y = -1
	elif(Input.is_action_pressed("Down")):
		up_was_pressed = false
		if(!down_was_pressed):
			down_was_pressed = true
			first_push_increase = (1+INITIAL_PUSH_DOWN);
	else:
		down_was_pressed = false
		up_was_pressed = false
		# apply gravity direction
		direction.y = 1

	# Check X direction & calculate velocity
	if(Input.is_action_pressed("Left")):
		direction.x = -4
		velocity.x = calculate_x(velocity,delta,direction);
	elif(Input.is_action_pressed("Right")):
		direction.x = 4
		velocity.x = calculate_x(velocity,delta,direction);
	print(velocity.x)
	# Calculate Y Velocity
	var direction_y = direction.y
	var increase_rate = 1
	if(!velocity.x):
		# if there's no speed, apply gravity only
		direction_y = 1;
	else:
		# else, increase direction relative to x velocity
		# but only if the player is pressing a direction
		if(Input.is_action_pressed("Up") || Input.is_action_pressed("Down")):
			increase_rate = (velocity.x / HORIZONTAL_TO_VERTICAL_RATIO)
	
	velocity.y+=(delta * (SPEED_INCREMENT_Y * (1+GRAVITY) * direction_y)*increase_rate)*first_push_increase
	velocity.y = clamp(velocity.y,-SPEED_MAX_Y,SPEED_MAX_Y)
	
	# Apply velocity to motion
	var motion = velocity * delta;
	var target = body.get_pos() + motion;
	
	# Check Upper Limit
	if(target.y <= LIMIT_UP):
		motion.y = LIMIT_UP;
		velocity.y = 0;
		target = body.get_pos() + motion
	elif(target.y >= LIMIT_DOWN):
		motion.y = 0;
		velocity.y = 0;
		target.y = LIMIT_DOWN;
	
	# Apply rotation
	#var rot = (atan2(target.x,target.y) + deg2rad(-25))*2
	#rot = clamp(rot,deg2rad(-20),deg2rad(60))
	#body.set_rot(rot)
	
	# Apply motion to body
	body.move(Vector2(0,motion.y))
	background.move_local_x(-motion.x)
	move_background(motion.x*10)
	
#check the player's height
func check_height(delta):
	#burn increases by 3
	if(body.get_pos().y <= 150):
		burn += .2
		
	elif (burn > 0 and body.get_pos().y < 150):
		#decrement burn by 1 if player descends
		burn -= .1
		
	#damage player and reset burn if it hits/exceeds 100
	if(burn >= 100):
		health -= 20
		set_life(health)
		burn = 0
		
	#set burn meter to burn value
	set_burn(burn)
	
func arrow_movement(delta):
	#arrow stuff - make a new arrow every few seconds
	spawn_timer += delta
	
	#track arrows spawned this round
	var spawned = 0
	
	if (spawn_timer > arrow_spawn_speed):
		
		#fire arrows
		while(spawned < arrow_spawn_frequency):
			#make an arrow
			create_arrow()
			#increment spawned
			spawned += 1
		
		spawn_timer = 0
		#speed up creation of arrows until a certain threshold is hit
		if(arrow_spawn_speed > arrow_spawn_threshold):
			arrow_spawn_speed -= arrow_spawn_decrement
		else:
			#reset arrow spawn speed and increase how many arrows are produced each round
			arrow_spawn_speed = 1
			arrow_spawn_frequency += 1
		
		#gradually increase arrow speed
		arrow_speed += delta
	
	var arrow_id = 0
	for arrow in arrow_arr:
		var arrow_position = get_node(arrow).get_pos()
		#arrows fly horizontally. Just to make life easier for now
		arrow_position.x -= arrow_speed * delta * 1.5
		get_node(arrow).set_pos(arrow_position)
		if(arrow_position.x < 0):
			get_node(arrow).free()
			arrow_arr.remove(arrow_id)
		arrow_id += 1

func create_arrow():
	var arrow_instance = arrow.instance()
	arrow_count += 1
	#make unique name for each arrow
	arrow_instance.set_name("arrow"+str(arrow_count))
	#add arrow to map
	add_child(arrow_instance)
	var arrow_position = get_node("arrow"+str(arrow_count)).get_pos()
	#position arrows randomly from right side of screen
	arrow_position.x = 1270
	arrow_position.y = rand_range(150, 800) 
	get_node("arrow"+str(arrow_count)).set_pos(arrow_position)
	#Add arrow to array of arrows
	arrow_arr.push_back("arrow"+str(arrow_count))

func calculate_x(velocity,delta,direction):
	var x = velocity.x + (delta * (SPEED_INCREMENT_X * direction.x))
	x = clamp(x,0,SPEED_MAX_X)
	return x;
	
func collision_detection():
	var arrow_id = 0
	for arrow in arrow_arr:
		var arrow_position = get_node(arrow).get_pos()
		if(arrow_position.y > body.get_pos().y - 20 and arrow_position.y < body.get_pos().y + 20):
			if(arrow_position.x <= body.get_pos().x - 15):
				if(arrow_position.x >= body.get_pos().x - 200):
					get_node(arrow).free()
					arrow_arr.remove(arrow_id)
					health -= 20
					set_life(health)
					print("Health: "+str(health))
		arrow_id += 1

#track player's health
func track_health():
	if (health <= 0):
		#go to game over screen upon reaching 0 health
		globals.setScene("res://Scenes/GameOver/GameOver.xml")
		