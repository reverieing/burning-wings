extends Node2D
var corner_top_left = Vector2(0,0)
var corner_bottom_right = Vector2(250,100)
var points_min = 20
var points_max = 100
var radius_min = 20
var radius_max = 50
var circles_diff = 1.1
var circles_min_multiplier = .3
func _draw():	
	randomize()
	draw_circles()

func get_random_point():
	var x = rand_range(corner_top_left.x,corner_bottom_right.x)
	var y = rand_range(corner_top_left.y,corner_bottom_right.y)
	return Vector2(x,y)

func draw_circles():
	var number_of_points = rand_range(points_min,points_max)
	var half = corner_bottom_right.x/2
	#draw_rect(Rect2(corner_top_left,corner_bottom_right),Color(1,0,0))
	for i in range(number_of_points):
		var point = get_random_point()
		var dist_from_center = max(half,point.x) - min(half,point.x)
		var weight_from_center = clamp((1-(dist_from_center/corner_bottom_right.y))*circles_diff,circles_min_multiplier,1)
		var radius_weight = rand_range(weight_from_center,1)
		var radius = rand_range(radius_min,radius_max)*radius_weight
		draw_circle(point,radius,Color(1,1,1))