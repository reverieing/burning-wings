extends Node2D

#Most of this code may be used/adapted elsewhere
var arrow = preload("res://bullet.xml")
var arrows = 0
var arrow_speed = 3
var arrow_spawn_speed = 2
var arrow_arr = []

#boat stuff
var boat = preload("res://boat.xml")
var boat_count = 0
var boat_arr = []
var spawn_timer = 0
const boat_speed = 1
const boat_spawn_speed = 5

func _ready():
	set_process(true)
	
#process calls functions for movement and collision
func _process(delta):
	enemy_movement(delta)
	#arrow_collision(delta)

#function to handle boat/arrow movement
func enemy_movement(delta):
	#arrow stuff
	var arrow_id = 0
	for arrow in arrow_arr:
		var arrow_position = get_node(arrow).get_pos()
		#update arrow position based on origin and target
		
		#arrow_position.x = arrow_position.x + arrow_speed * delta
		#get_node(arrow).set_pos(arrow_position)
		#seek_target()
		
		#remove from screen if it goes off-screen
		if(arrow_position.x < get_viewport().get_rect().x):
			get_node(arrow).free()
			arrow_arr.remove(arrow_id)
		elif(arrow_position.x > get_viewport().get_rect().width):
			get_node(arrow).free()
			arrow_arr.remove(arrow_id)
		elif(arrow_position.y > get_viewport().get_rect().y):
			get_node(arrow).free()
			arrow_arr.remove(arrow_id)
		elif(arrow_position.y > get_viewport().get_rect().height):
			get_node(arrow).free()
			arrow_arr.remove(arrow_id)
		arrow_id += 1
	
	#boat stuff - make a new one every 5 seconds
	if (spawn_timer > boat_spawn_speed):
		create_boat()
		spawn_timer = 0
	
	var boat_id = 0
	for (boat in boat_arr):
		var boat_position = get_node(boat).get_pos()
		#boats sail vertically. Just to make life easier for now
		boat_position.y += (boat_speed + delta)
		
		if(boat_position.y > get_viewport().get_rect().y):
			get_node(boat).free()
			boat_arr.remove(arrow_id)
		elif(boat_position.y > get_viewport().get_rect().height):
			get_node(boat).free()
			boat_arr.remove(boat_id)
		boat_id += 1
	
func create_arrow():
	var arrow_instance = arrow.instance()
	arrows += 1
	#make unique name for each arrow
	arrow_instance.set_name("arrow"+str(arrows))
	#add arrow to map
	add_child(arrow_instance)
	var arrow_position = get_node("arrow"+str(arrows)).get_pos()
	#start position based on boat's position?
	var boat_position
	
	#arrow_position.x = 
	#arrow_position.y =
	get_node("arrow"+str(arrows)).set_pos(arrow_position)
	#Add arrow to array of arrows
	arrow_arr.push_back("arrow"+str(arrows))
	
	#target is player
	var target_position = find_node("Player").get_pos()
	
func create_boat():
	var boat_instance = boat.instance()
	boat_count += 1
	#make unique name for each boat
	boat_instance.set_name("boat"+str(boat_count))
	#add arrow to map
	add_child(boat_instance)
	var boat_position = get_node("boat"+str(boat_count)).get_pos()
	#position boats randomly on screen
	boat_position.x = rand_range(get_viewport().get_rect().x, get_viewport().get_rect().width) 
	boat_position.y = rand_range(get_viewport().get_rect().y, get_viewport().get_rect().height) 
	get_node("boat"+str(boat_count)).set_pos(boat_position)
	#Add boat to array of boats
	boat_arr.push_back("boat"+str(boat_count))
	