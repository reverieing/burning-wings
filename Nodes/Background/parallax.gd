var layers = [];
export var speed = 0;
var parallax;

func _ready():
	set_process(true)
	_parallax_start()
	
func _parallax_start():
	parallax = self.get_node("ParallaxBackground");
	layers.append(parallax.get_node("Water"))
	layers.append(parallax.get_node("Clouds1"))
	layers.append(parallax.get_node("Clouds2"))
	layers.append(parallax.get_node("Sky"))
	
func _process(delta):
	for i in range(layers.size()):
		moveLayer(layers[i],delta,i+1)
	
func moveLayer(layer,delta,pos):
	var curPos = layer.get_pos();
	
	if(curPos.x <= -2550):
		curPos.x = 0
	
	curPos.x -= speed * delta * pos
	layer.set_pos(curPos)


