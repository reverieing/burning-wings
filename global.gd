extends Node

#values are global in scope
var currentScene = null
var timeElapsed = 0

func _ready():
	#set currentScene
	currentScene = get_tree().get_root().get_child(get_tree().get_root().get_child_count() - 1)
	#find_node("WorldTimer").start
	set_process(true)
	
#constantly update timer
func _process(delta):
	timeElapsed += delta
	
#function to set scene to a scene that is passed in later
func setScene(scene):
	#purge current scene
	currentScene.queue_free()
	var s = ResourceLoader.load(scene)
	#create instance of sceen
	currentScene = s.instance()
	get_tree().get_root().add_child(currentScene)
	