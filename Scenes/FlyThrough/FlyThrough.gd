extends Node2D
var Waves
var position = Vector2()
var desired_speed = Vector2()
var limit_speed_up = Vector2(5,.3)
var limit_speed_down = Vector2(0.01,-3)
var acceleration = Vector2(.03,1)
var damping = Vector2(.01,.3)
onready var Cloud_Scene = preload("res://Nodes/Cloud/Cloud.xml")
onready var Clouds = get_node("Clouds")
var clouds = []

func clamp_vector(v,_min,_max):
	v.x = clamp(v.x,_min.x,_max.x)
	v.y = clamp(v.y,_min.y,_max.y)
	return v

func random_point_on_screen():
	var rect = get_viewport_rect();
	return Vector2(rand_range(0,rect.size.x),rand_range(0,rect.size.y))

func _ready():
	Waves = get_node("Waves")
	Waves.spawn(10)
	for i in range(0,40):
		var cloud = Cloud_Scene.instance()
		Clouds.add_child(cloud)
		var scale = rand_range(.1,.5)
		cloud.set_scale(Vector2(scale,scale))
		cloud.set_global_pos(random_point_on_screen())
	set_fixed_process(true)
	
func _fixed_process(delta):
	move(delta)
	animate_waves(delta)

func animate_waves(delta):
	position = (position - desired_speed)/5
	pass;
	
func interpolate(v1,v2,v3):
	v1.x+=(v2.x - v1.x) * v3.x
	v1.y+=(v2.y - v1.y) * v3.y
	return v1
	
func move(delta):
	
	if Input.is_action_pressed("Left"):
		desired_speed.x = -acceleration.x
	elif Input.is_action_pressed("Right"):
		desired_speed.x = acceleration.x
	else:
		desired_speed.x = 0
		
	if Input.is_action_pressed("Up") && !Waves.reached_limit_up:
		desired_speed.y = acceleration.y
	elif Input.is_action_pressed("Down") && !Waves.reached_limit_down:
		desired_speed.y = -acceleration.y
	else:
		desired_speed.y = 0
		
	var waves_speed = interpolate(Waves.speed,desired_speed,damping)
	Waves.speed = clamp_vector(waves_speed,limit_speed_down,limit_speed_up)
	