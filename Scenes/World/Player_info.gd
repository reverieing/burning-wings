
extends KinematicBody2D

var player_pos = self.get_pos()
var playerHealth = 3
var invuln_timer = 2
#Player has limited invincibility after being hit
var mercyFrames = false

func _ready():
	set_process(true)
	
func getPlayerHealth():
	return playerHealth
	
func damage_player():
	playerHealth -= 1
	mercyFrames = true
	#player cannot be harmed for a short while after being hit
	invuln_timer = 0
	print(playerHealth)
	if(invuln_timer >= 3):
		mercyFrames = false

func _process(delta):
	invuln_timer += 1
	
func _on_player_area2D_area_enter_shape( area_id, area, area_shape, area_shape ):
	if(playerHealth > 0 and !mercyFrames):
		#call damage_player
		damage_player()
		
	print("player area collision")
		
#when mercyTimer runs out
func _on_Timer_timeout():
	#turn off invincibility
	mercyFrames = false

func _on_player_area2D_body_enter( body ):
	body.queue_free()
	print("player body collision")
