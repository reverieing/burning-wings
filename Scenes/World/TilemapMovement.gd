extends Node

var speed = 8;
var velocity = Vector2();

func _ready():
	set_fixed_process(true)
	
func _fixed_process(delta):	
	if(Input.is_action_pressed("Left")):
		velocity.x = speed
	elif(Input.is_action_pressed("Right")):
		velocity.x = -speed 
	
	if(Input.is_action_pressed("Up")):
		velocity.y = 0
	elif(Input.is_action_pressed("Down")):
		velocity.y = -speed * .75
	else:
		velocity.y = 0
		
	move(velocity)
	