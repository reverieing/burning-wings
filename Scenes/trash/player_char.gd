
extends KinematicBody2D

var speed = 4;

var velocity = Vector2()
var max_loc = Vector2()
var player_pos = self.get_pos()
var playerHealth = 3
var invuln_timer = 2

#Player has limited invincibility after being hit
var mercyFrames = false

func getPlayerHealth():
	return playerHealth

func _ready():
	
	max_loc.x = OS.get_window_size().width
	max_loc.y = OS.get_window_size().height
	move_to(Vector2(max_loc.x/2, max_loc.y/2))
	
	set_fixed_process(true)
	set_process(true)

func _process(delta):
	invuln_timer += 1
	
func _fixed_process(delta):
	
	var screen_x = get_viewport().get_rect().pos.x
	var screen_y = get_viewport().get_rect().pos.y
	var screen_width = OS.get_window_size().width * 2
	var screen_height = OS.get_window_size().height
	
	#alter velocity based on input
	if(Input.is_action_pressed("Left")):
		if(speed > 4):
			speed -= .2
		
		velocity.x = -speed
		
		if(player_pos.x <= screen_x):
			velocity.x = 0
		
			
	elif(Input.is_action_pressed("Right") and get_pos().x < screen_width):
		if(speed <= 16):
			speed += .2
		
		velocity.x = speed
		
		if(player_pos.x >= screen_width):
			velocity.x = 0
		
	else:
		velocity.x = 0
	
	if(Input.is_action_pressed("Up")):
			
		if(speed >=2):
			velocity.y = -speed * .75
	elif(Input.is_action_pressed("Down") and speed >=2):
		
		if(speed >= 2):
			velocity.y = speed * .75
	else:
		velocity.y = 0
	
	
		
	#move based on velocity
	move(velocity)

func _on_player_area2D_area_enter_shape( area_id, area, area_shape, area_shape ):
	if(playerHealth > 0 and !mercyFrames):
		#decrease health
		playerHealth -= 1
		mercyFrames = true
		#player cannot be harmed for a short while after being hit
		invuln_timer = 0
		print(playerHealth)
	if(invuln_timer >= 3):
		mercyFrames = false
	print("player area collision")
		
#when mercyTimer runs out
func _on_Timer_timeout():
	#turn off invincibility
	mercyFrames = false

func _on_player_area2D_body_enter( body ):
	body.queue_free()
	print("player body collision")