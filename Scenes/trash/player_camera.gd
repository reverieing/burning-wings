
extends Camera2D

# member variables here, example:
# var a=2
# var b="textvar"
const SPEED = 6;

var velocity = Vector2()

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	#set_fixed_process(true)
	pass

func _fixed_process(delta):
	
	#alter velocity based on input
	if(Input.is_action_pressed("A")):
		velocity.x = -SPEED
	elif(Input.is_action_pressed("D")):
		velocity.x = SPEED
	else:
		velocity.x = 0
	
	if(Input.is_action_pressed("W")):
		velocity.y = -SPEED
	elif(Input.is_action_pressed("S")):
		velocity.y = SPEED
	else:
		velocity.y = 0
	#move based on velocity
	move_local_x(velocity.x)
	move_local_y(velocity.y)
	#move(velocity)
