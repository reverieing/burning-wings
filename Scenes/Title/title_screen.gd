
extends ParallaxLayer

var globals

func _ready():
	# Called every time the node is added to the scene.
	# Initialization here
	globals = get_node("/root/global")
	set_process(true)
	
func _process(delta):
	#space or enter progresses to next scene
	if(Input.is_action_pressed("ui_accept") or Input.is_action_pressed("ui_select")):
		globals.setScene("res://Nodes/PaperPlane/paperPlane.xml")
		
