extends Label

var posX
var posY

func _ready():
	posX = get_viewport().get_rect().size.width/2 - 100
	posY = get_viewport().get_rect().size.height/2 + 40
	var labelPosition = Vector2(posX, posY)
	set_pos(labelPosition)
	set_fixed_process(true)
	
func fixed_process(delta):
	posX = get_viewport().get_rect().size.width/2 - 100
	posY = get_viewport().get_rect().size.height/2 + 40
	var labelPosition = Vector2(posX, posY)
	
	set_pos(labelPosition)