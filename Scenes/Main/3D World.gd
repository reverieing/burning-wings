var material_type = FixedMaterial.PARAM_DIFFUSE
var surface = MeshInstance
var texture
export var curvature = 0

func _ready():
	set_process(true)
	texture = get_parent().get_node("Viewport").get_render_target_texture()
	surface = self.get_node("WorldPlane")
	var material = surface.get_material_override()
	material.set_texture(material_type, texture)
	
func _process(delta):
	#surface.set("morph/Shrinkwrap",1)