#extends Spatial
#extends MeshInstance
var material_type = FixedMaterial.PARAM_DIFFUSE

func _ready():
	var texture = get_parent().get_node("Viewport").get_render_target_texture()
	#var surface = get_node("Quad")
	#var material = surface.get_material_override()
	var material = self.get_material_override()
	material.set_texture(material_type, texture)