extends Node

var tileMap = TileMap;
const SPEED = 8;
const Boat_Tile_index = 1;
const ratio_2d_3d_x = 1.66
const ratio_2d_3d_y = 2.9
const ratio_2d_3d_movement_x = .055;
const ratio_2d_3d_movement_y = .09;
const pos_z = -9;
var velocity = Vector2();
var Player = Sprite3D;
var boat_scene = preload("res://Nodes/BoatArea/boat.xml")
var Boats = Sprite3D;

func _ready():
	set_fixed_process(true)
	tileMap = self.get_node("Viewport/World/ParallaxBackground/TileMap")
	Boats = self.get_node("Boats")
	Player = self.get_node("Player")
	var tiles = tileMap.get_used_cells();
	for i in range(tiles.size()):
		var x = tiles[i][0];
		var y = tiles[i][1];
		var tile_index = tileMap.get_cell(x,y);
		if(tile_index == Boat_Tile_index):
			spawn_boat(x,y);
	
func spawn_boat(x,y):
	var pos_x = x * ratio_2d_3d_x
	var pos_y = y * ratio_2d_3d_y
	var position = Vector3(pos_x,pos_z,pos_y)
	var boat = boat_scene.instance()
	boat.translate(position)
	Boats.add_child(boat)
	
func _fixed_process(delta):
	move();
	
func move():
	var map_x = tileMap.get_global_pos().x
	var map_y = tileMap.get_global_pos().y
	
	if(Input.is_action_pressed("Left")):
		velocity.x = SPEED
	elif(Input.is_action_pressed("Right")):
		velocity.x = -SPEED
	else:
		velocity.x = 0
	
	if(Input.is_action_pressed("Up")):
		velocity.y = SPEED
	elif(Input.is_action_pressed("Down")):
		velocity.y = -SPEED
	else:
		velocity.y = 0
	
	var boats_position = Vector3(velocity.x*ratio_2d_3d_movement_x,0,velocity.y*ratio_2d_3d_movement_y);
	Boats.move(boats_position)
	tileMap.move_local_x(velocity.x);
	tileMap.move_local_y(velocity.y);
	