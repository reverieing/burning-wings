
extends Node2D

var globals

func _ready():
	globals = get_node("/root/global")
	set_process(true)

func _process(delta):
	#space or enter returns to title screen
	if(Input.is_action_pressed("ui_accept") or Input.is_action_pressed("ui_select")):
		globals.setScene("res://Scenes/Title/title_screen.xml")

